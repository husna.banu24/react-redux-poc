import { combineEpics } from 'redux-observable';
import { Observable, BehaviorSubject } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { ajax } from 'rxjs/observable/dom/ajax';
import { ofType } from 'redux-observable';

import {
    FETCH_EMP_DETAILS,
    fetchdetailsuccess,
    fetchdetailsFailure
} from "../actions";

const fetchEmpDetails = action$ => action$.pipe(
  ofType(FETCH_EMP_DETAILS),
  mergeMap(action =>
    ajax.getJSON(`https://reqres.in/api/users/${action.payload}`).
      map(data => data).
      map(details => ([
        {id: details.data.id,name: details.data.first_name +' '+ details.data.last_name,image: details.data.avatar}
      ])).map(details => fetchdetailsuccess(details))
      .catch(error => Observable.of(fetchdetailsFailure(error.message)))

  )
);

/*function fetchEmpDetails(action$,store) {
  console.log('------------'+action$);
  var id = action$.id;
  return action$
    .ofType(FETCH_EMP_DETAILS)
    .switchMap((action) => {
      return ajax
        .getJSON(url+action.payload)
        .map(data => data)
        .map(details => ([
          {id: details.data.id,name: details.data.first_name +' '+ details.data.last_name,image: details.data.avatar}
        ]))
    })
    .map(details => fetchdetailsuccess(details))
    .catch(error => Observable.of(fetchdetailsFailure(error.message)))
}*/

//export const rootEpic = combineEpics(fetchEmpDetails);

const epic$ = new BehaviorSubject(combineEpics(fetchEmpDetails));
export const rootEpic = (action$, state$) => epic$.pipe(
  mergeMap(epic => epic(action$, state$))
);
