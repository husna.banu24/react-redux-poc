import {
    FETCH_EMP_DETAILS,
    FETCH_EMP_DETAILS_SUCCESS,
    FETCH_EMP_DETAILS_FAILURE,
    CLEAR_EMP_DETAILS
} from '../actions';

const initialState = {
    details: [],
    isLoading: true,
    error: null
};

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_EMP_DETAILS:
            return {
                ...state,
                isloading: true,
                error: null
            };
        case FETCH_EMP_DETAILS_SUCCESS:
              console.log(action)
                return {
                    details: [...action.payload],
                    // whenever the fetching finishes, we stop showing the spinner and then show the data
                    isLoading: false,
                    error: null
                };
        case FETCH_EMP_DETAILS_FAILURE:
                return {
                    details: [],
                    isLoading: false,
                    // same as FETCH_WHISKIES_SUCCESS, but instead of data we will show an error message
                    error: action.payload
                };
        case CLEAR_EMP_DETAILS:
                return {
                    details: [],
                    isLoading:true,
                    error:null
                };
        default:
            return state;
    }
}
