import React from 'react';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import  DetailDiv  from './DetailDiv';
import Header from './header';

export default class extends React.PureComponent{
  render()
  {
    return(
    <Stack>
      <Header/>
      <DetailDiv/>
    </Stack>)
  }
}
