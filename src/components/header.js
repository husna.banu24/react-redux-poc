import React from 'react';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import { mergeStyleSets, DefaultPalette } from 'office-ui-fabric-react/lib/Styling';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { DefaultButton} from 'office-ui-fabric-react/lib/Button';
import { Dropdown} from 'office-ui-fabric-react/lib/Dropdown';
import { BaseComponent} from 'office-ui-fabric-react/lib/Utilities';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getEmpDetails , clearEmpDetails} from '../actions';

initializeIcons();

class Header extends BaseComponent{
    constructor(props) {
      super(props)
      this.state = {
        selectedItem: {},
        selectedItems: [],
        selectedEmpId:{},
        EmpidDrop:true,
        getDetails:true
      };
      }
    changeDept = (event, item) => {
      this.setState({ selectedItem: item ,EmpidDrop:false});
    }
    selectId = (event, item) => {
      this.setState({ getDetails: false, selectedEmpId:item});
    }
    getEmpdet = () => {
      this.props.getEmpDetails(this.state.selectedEmpId);
    }

    render(){
      const styles = mergeStyleSets({
        root: {
          background: DefaultPalette.themeTertiary
        },
        root1:{
          background:DefaultPalette.themeGrey
        },
        item: {
          color: DefaultPalette.white,
          background: DefaultPalette.themePrimary,
          padding: 5
        }
      });
      const { selectedItem, EmpidDrop, getDetails, selectedEmpId } = this.state;
      const empIds = (selectedItem.key === "B")?[
                      { key: 'A', text: '1' },
                      { key: 'B', text: '2' },
                      { key: 'C', text: '3' },
                      { key: 'D', text: '4' },
                      { key: 'E', text: '5' },
                    ]:
                    [
                    { key: 'F', text: '6' },
                    { key: 'G', text: '7' },
                    { key: 'H', text: '8' },
                    { key: 'I', text: '9' },
                    { key: 'J', text: '10' },]
        const {
          clearEmpDetails,
        } = this.props;

      return(
        <Stack horizontal disableShrink gap={5} padding={10} className={styles.root} >
            <Stack.Item align="auto" className={styles.item}>
              <Dropdown
                selectedKey={selectedItem ? selectedItem.key : undefined}
                placeholder="Department"
                onChange={this.changeDept}
                errorMessage=""
                options={[
                  { key: 'A', text: 'ENGINEERING' },
                  { key: 'B', text: 'HR' }
                ]}
              />
            </Stack.Item>
            <Stack.Item align="auto" className={styles.item}>
              <Dropdown
                selectedKey={selectedEmpId ? selectedEmpId.key : undefined}
                placeholder="Employee ID"
                disabled={EmpidDrop}
                options={empIds}
                onChange={this.selectId}
              />
            </Stack.Item>
          <Stack.Item align="baseline" className={styles.item}>
            <DefaultButton
              data-automation-id="test"
              allowDisabledFocus={true}
              disabled={getDetails}
              checked={false}
              text="Get Details"
              onClick={this.getEmpdet}
            />
          </Stack.Item>
          <Stack.Item align="baseline" className={styles.item}>
            <DefaultButton
              data-automation-id="test"
              allowDisabledFocus={true}
              disabled={false}
              checked={false}
              text="Clear"
              onClick={clearEmpDetails}
            />
          </Stack.Item>
        </Stack>
      )
    }
  }

  const mapStateToProps = state => ({ ...state });

  const mapDispatchToProps = dispatch =>
      bindActionCreators({
          getEmpDetails,
          clearEmpDetails
      }, dispatch);

  export default connect(mapStateToProps, mapDispatchToProps)(Header);
