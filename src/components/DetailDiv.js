import React from 'react';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import { mergeStyleSets, DefaultPalette } from 'office-ui-fabric-react/lib/Styling';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { BaseComponent} from 'office-ui-fabric-react/lib/Utilities';
import { Image } from 'office-ui-fabric-react/lib/Image';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import { connect } from 'react-redux';

initializeIcons();

class DetailDiv extends BaseComponent{
    constructor(props) {
      super(props)
      this.state = {
        selectedItem: {},
        selectedItems: []
      };
      }

    render(){
      const styles = mergeStyleSets({
        root: {
          background: DefaultPalette.themeTertiary
        },
        root1:{
          background:DefaultPalette.themeGrey
        },
        item: {
          color: DefaultPalette.white,
          background: DefaultPalette.themePrimary,
          padding: 5
        }
      });

      const {
        isLoading,
        details
      } = this.props;

      var id,name,imgSrc;
      details.map(function(index,value){
         id = index.id;
         name = index.name;
         imgSrc = index.image
      })
      return(
        <Stack>
          <Stack disableShrink gap={5} padding={10} className={styles.root1} >
          {(isLoading?<Spinner size={SpinnerSize.large } />:'')}
            <Stack.Item align="start" className={styles.root1}>
              <Image
              src={imgSrc}
              alt="Example implementation of the property image fit using the cover value on an image taller than the frame."
              width={700}
              height={400}
              />
            </Stack.Item>
          </Stack>
          <Stack horizontal disableShrink gap={500} padding={10} className={styles.root} >
            <Stack.Item align="auto" className={styles.item}>
              <div>
              ID:{id}
              </div>
            </Stack.Item>
            <Stack.Item align="auto" className={styles.item}>
              <div>
              Name:{name}
              </div>
            </Stack.Item>
          </Stack>
        </Stack>
      )
    }
  }

const mapStateToProps = state => ({ ...state });

export default connect(mapStateToProps)(DetailDiv);
