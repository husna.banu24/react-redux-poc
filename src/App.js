import React, { Component } from 'react';
import './App.css';
import Detail from './components/Employee_detail';


export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Detail/>
      </div>
    );
  }
}
