export const FETCH_EMP_DETAILS = 'FETCH_EMP_DETAILS';
export const FETCH_EMP_DETAILS_SUCCESS = 'FETCH_EMP_DETAILS_SUCCESS';
export const FETCH_EMP_DETAILS_FAILURE = 'FETCH_EMP_DETAILS_FAILURE';
export const CLEAR_EMP_DETAILS = 'CLEAR_EMP_DETAILS';

export const getEmpDetails = (id) => {
  return {
    type:FETCH_EMP_DETAILS,
    payload:id.text
  }
}

export const fetchdetailsuccess = (details) => ({
    type: FETCH_EMP_DETAILS_SUCCESS,
    payload: details
});

export const fetchdetailsFailure = (message) => ({
    type: FETCH_EMP_DETAILS_FAILURE,
    payload: message
});

export const clearEmpDetails = () => ({
  type: CLEAR_EMP_DETAILS
})
